package com.botscrew.books.view;

import java.util.Scanner;

public class Input {
    public static String enterCommand() {
        Scanner sc = new Scanner(System.in);
        String res = sc.nextLine();
        return res;
    }
}
