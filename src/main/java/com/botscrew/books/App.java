package com.botscrew.books;

import org.apache.log4j.Logger;

import static com.botscrew.books.model.console_commands.CheckCommand.check;
import static com.botscrew.books.view.Input.enterCommand;

import java.util.Scanner;

/**
 * Hello world!
 */
public class App {
    private static Logger LOG = Logger.getLogger(App.class);
    public static void main(String[] args) {
        LOG.info("You can use such commands: \n" +
                "add <Author> \"<Book Name>\"" + "\n" +
                "remove <Book Name>" + "\n" +
                "all books" + "\n" +
                "edit <Book Name>" + "\n" +
                "clean books" + "\n"
        );
        LOG.info("Go typing something:)");
        char ch;
        do {
            String line = enterCommand();
            check(line);
            System.out.println("Other command? Y/N");
            Scanner sc = new Scanner(System.in);
            ch = sc.next().charAt(0);
        } while (ch == 'Y' || ch == 'y');

    }
}
