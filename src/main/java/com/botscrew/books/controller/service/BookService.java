package com.botscrew.books.controller.service;

import com.botscrew.books.controller.dao.implementation.BookDaoImpl;
import com.botscrew.books.model.Book;

import java.sql.SQLException;
import java.util.List;

public class BookService {
    public void create(Book book) {
        new BookDaoImpl().create(book);
    }

    public void deleteByName(String name) {
        new BookDaoImpl().deleteByName(name);
    }

//    public Book getBookByName(String name) {
//        return new BookDaoImpl().getBookByName(name);
//    }

    public void editBook(String oldName, String newName) {
        new BookDaoImpl().editBook(oldName, newName);
    }


    public List<Book> getAllBooks() {
        return new BookDaoImpl().getAllBooks(null);
    }

    public void deleteAllBooks() {
        new BookDaoImpl().deleteAllBooks();
    }
}
