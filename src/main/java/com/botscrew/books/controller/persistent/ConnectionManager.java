package com.botscrew.books.controller.persistent;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionManager {
    private static String url = "jdbc:sqlite:books.db";


    private static Connection connection = null;

    public static Connection getConnection() {
        if (connection == null) {
            initializeConnection();
        }
        return connection;
    }

    private static void initializeConnection() {
        try {
            connection = DriverManager.getConnection(url);
           /* Statement statement = connection.createStatement();
            statement.execute("CREATE TABLE IF NOT EXISTS 'books'(" +
                    "'id' INTEGER PRIMARY KEY AUTO_INCREMENT," +
                    "'name' VARCHAR(30)," +
                    "'author' VARCHAR(30)" +
                    ");");*/
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
