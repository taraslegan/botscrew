package com.botscrew.books.controller.transformer;

import com.botscrew.books.model.Book;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BookTransformer {
    public Book fromResultSetToObject(ResultSet rs) throws SQLException {
        Book result = null;

        result = new Book();
        result.setName(rs.getString("name"));
        result.setAuthor(rs.getString("author"));

        return result;
    }
}
