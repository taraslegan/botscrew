package com.botscrew.books.controller.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {
    public boolean addValidate(String line) {
        Pattern p = Pattern.compile("add ([a-zA-Z]|\\.|\\s)+ \"([a-zA-Z]|\\.|\\s)+\"");
        Matcher matcher = p.matcher(line);
        if (matcher.matches()) return true;
        else return false;
    }

    public boolean removeValidate(String line) {
        Pattern p = Pattern.compile("remove ([a-zA-Z]|\\.|\\s)+");
        Matcher matcher = p.matcher(line);
        if (matcher.matches()) return true;
        else return false;
    }

    public boolean allBooksValidate(String line) {
        Pattern p = Pattern.compile("all books\\s*");
        Matcher matcher = p.matcher(line);
        if (matcher.matches()) return true;
        else return false;
    }

    public boolean editBookValidate(String line) {
        Pattern p = Pattern.compile("edit ([a-zA-Z]|\\.|\\s)+");
        Matcher matcher = p.matcher(line);
        if (matcher.matches()) return true;
        else return false;
    }

    public boolean deleteBooksValidate(String line){
        Pattern p = Pattern.compile("clean books\\s*");
        Matcher matcher = p.matcher(line);
        if (matcher.matches()) return true;
        else return false;
    }
}
