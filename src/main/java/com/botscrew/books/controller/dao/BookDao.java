package com.botscrew.books.controller.dao;

import com.botscrew.books.model.Book;

import java.sql.SQLException;
import java.util.List;

public interface BookDao {
    void create(Book book) throws SQLException;
    void deleteByName(String name) throws SQLException;
//    Book getBookByName(String name) throws SQLException;
    List<Book> getAllBooks(String name) throws SQLException;
    void editBook(String oldName, String newName) throws SQLException;
    void deleteAllBooks() throws SQLException;
}
