package com.botscrew.books.controller.dao.implementation;

import com.botscrew.books.controller.dao.BookDao;
import com.botscrew.books.controller.persistent.ConnectionManager;
import com.botscrew.books.controller.transformer.BookTransformer;
import com.botscrew.books.model.Book;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.botscrew.books.view.Input.enterCommand;

public class BookDaoImpl implements BookDao {
    private final String DELETE_BOOK_BY_NAME = "DELETE FROM books WHERE name = ?";
    private final String DELETE_BOOK_BY_AUTHOR = "DELETE FROM books WHERE author = ?";
    private final String GET_BOOK_BY_NAME = "SELECT * FROM books WHERE name = ?";
    private final String GET_ALL_BOOKS = "SELECT * FROM books ORDER BY name;";
    private final String INSERT_BOOK = "INSERT INTO books(author, name) VALUES(?,?);";
    private final String CHANGE_BOOK_NAME = "UPDATE books SET name = ? WHERE name = ?;";
    private final String DELETE_ALL_BOOKS = "DELETE FROM books;";
    private Logger LOG = Logger.getLogger(BookDao.class);

    @Override
    public void editBook(String oldName, String newName) {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement statement = conn.prepareStatement(CHANGE_BOOK_NAME)) {
            statement.setString(1, newName);
            statement.setString(2, oldName);
            statement.executeUpdate();
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
    }

    @Override
    public void create(Book book) {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement statement = conn.prepareStatement(INSERT_BOOK)) {
            statement.setString(1, book.getAuthor());
            statement.setString(2, book.getName());
            statement.executeUpdate();
            LOG.info("book " + book.getAuthor() + " \"" + book.getName() + "\" " + "was added");
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
    }

    @Override
    public void deleteByName(String name) {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement statement = conn.prepareStatement(DELETE_BOOK_BY_NAME)) {
            List<Book> allBooks = getAllBooks(name);
            if (allBooks.isEmpty()) {
              LOG.info("There is no book with such name!");
            } else if (allBooks.size() == 1) {
                statement.setString(1, allBooks.get(0).getName());
                statement.executeUpdate();
                LOG.info("book " + allBooks.get(0).getAuthor() + " \"" + allBooks.get(0).getName() + "\"" + " was removed");
            } else {
                LOG.info("we have few books with such name please choose one by typing a number of book:\n");
                int counter = 0;
                for (Book b : allBooks) {
                    System.out.print(++counter + ". " + b.toString());
                }
                try (PreparedStatement preparedStatement = conn.prepareStatement(DELETE_BOOK_BY_AUTHOR)) {
                    Integer number = Integer.parseInt(enterCommand());
                    Book bookToRemove = allBooks.get(number - 1);
                    preparedStatement.setString(1, bookToRemove.getAuthor());
                    preparedStatement.executeUpdate();
                    LOG.info("book " + bookToRemove.getAuthor() + " \"" + bookToRemove.getName() + "\"" + " was removed");
                }

            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
    }

//    @Override
//    public Book getBookByName(String name) {
//        Connection conn = ConnectionManager.getConnection();
//        Book book = new Book();
//        try (PreparedStatement statement = conn.prepareStatement(GET_BOOK_BY_NAME)) {
//            statement.setString(1, name);
//            ResultSet rs = statement.executeQuery();
//            while (rs.next()) {
//                book = new BookTransformer().fromResultSetToObject(rs);
//            }
//        } catch (SQLException e) {
//            LOG.error(e.getMessage());
//        }
//
//        return book;
//    }

    @Override
    public List<Book> getAllBooks(String name) {
        List<Book> books = new ArrayList<>();
        Connection conn = ConnectionManager.getConnection();

        try (Statement statement = conn.createStatement()) {
            if (name == null) {
                ResultSet rs = statement.executeQuery(GET_ALL_BOOKS);
                while (rs.next()) {
                    books.add(new BookTransformer().fromResultSetToObject(rs));
                }
            } else {
                try (PreparedStatement preparedStatement = conn.prepareStatement(GET_BOOK_BY_NAME)) {
                    preparedStatement.setString(1, name);
                    ResultSet rs2 = preparedStatement.executeQuery();
                    while (rs2.next()) {
                        books.add(new BookTransformer().fromResultSetToObject(rs2));
                    }
                }

            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return books;
    }

    @Override
    public void deleteAllBooks() {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement statement = conn.prepareStatement(DELETE_ALL_BOOKS)) {
            statement.executeUpdate();
            LOG.info("All books have just deleted!");
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
    }
}
