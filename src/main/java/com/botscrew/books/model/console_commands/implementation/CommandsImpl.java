package com.botscrew.books.model.console_commands.implementation;

import com.botscrew.books.controller.service.BookService;
import com.botscrew.books.model.Book;
import com.botscrew.books.model.console_commands.Commands;
import org.apache.log4j.Logger;

import static com.botscrew.books.view.Input.enterCommand;

import java.sql.SQLException;
import java.util.List;

public class CommandsImpl implements Commands {
    private Logger LOG = Logger.getLogger(CommandsImpl.class);
    private BookService bookService;

    public CommandsImpl() {
        bookService = new BookService();
    }

    @Override
    public void addBook(String line) {
        String name = line.substring(line.indexOf("\"") + 1, line.lastIndexOf("\""));//name
        String author = line.substring(line.indexOf(" ") + 1, line.indexOf("\"")).trim();//author
        Book book;
        book = new Book(author, name);
        bookService.create(book);
    }

    @Override
    public void removeBook(String line) {
        String name = line.substring(line.indexOf(" ") + 1, line.length()).trim();//name
        bookService.deleteByName(name);
    }

    @Override
    public void editBook(String line) {
        String oldName = line.substring(line.indexOf(" ") + 1, line.length()).trim();//name
        System.out.println("new name: ");
        String newNameOfTheBook = enterCommand();
        bookService.editBook(oldName, newNameOfTheBook);
    }

    @Override
    public List<Book> allBooks() {
        return bookService.getAllBooks();
    }

    @Override
    public void deleteAllBooks() {
        bookService.deleteAllBooks();
    }
}
