package com.botscrew.books.model.console_commands;

import com.botscrew.books.model.Book;

import java.sql.SQLException;
import java.util.List;

public interface Commands {
    void addBook(String line);
    void removeBook(String line);
    void editBook(String line);
    List<Book> allBooks();
    void deleteAllBooks();
}
