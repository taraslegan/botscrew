package com.botscrew.books.model.console_commands;

import com.botscrew.books.controller.validation.Validator;
import com.botscrew.books.model.Book;
import com.botscrew.books.model.console_commands.implementation.CommandsImpl;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.List;

public class CheckCommand {

    private static Logger LOG = Logger.getLogger(CheckCommand.class);
    private static Commands command = new CommandsImpl();
    private static Validator validator = new Validator();

    public static void check(String wholeLine) {
        String firstWord = wholeLine.substring(0, wholeLine.indexOf(" ") + 1).trim();
        switch (firstWord) {
            case "add":
                if (validator.addValidate(wholeLine)) {
                    command.addBook(wholeLine);
                } else {
                    LOG.info("Wrong format for add command!");
                }
                break;
            case "remove":
                if (validator.removeValidate(wholeLine)) {
                    command.removeBook(wholeLine);

                } else {
                    LOG.info("Wrong format for remove command!");
                }
                break;
            case "edit":

                if (validator.editBookValidate(wholeLine)) {
                    command.editBook(wholeLine);
                } else {
                    LOG.info("Wrong format for edit command!");
                }
                break;
            case "all":
                if (validator.allBooksValidate(wholeLine)) {
                    List<Book> books = command.allBooks();
                    if (books.size() == 0) LOG.info("There are no books!");
                    else {
                        LOG.info("Our books:");
                        for (Book b : books) {
                            System.out.print(b.toString());
                        }
                    }
                } else {
                    LOG.info("Wrong format for all books command!");
                }

                break;
            case "clean":
                if (validator.deleteBooksValidate(wholeLine)) {
                    command.deleteAllBooks();

                } else {
                    LOG.info("Wrong format for clean command!");
                }
                break;
            default:
                LOG.info("There are no such command!");
        }
    }
}
