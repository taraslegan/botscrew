package com.botscrew.books.model;

public class Book {
    private String author;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Book() {
    }

    public Book(String author, String name) {
        this.name = name;
        this.author = author;
    }

    @Override
    public String toString() {
        return author + " \"" + name + "\"\n";
    }
}
